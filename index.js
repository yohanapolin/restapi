const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;

app.use(bodyParser.json());

app.post('/post', (req, res) => {
    res.send({
        message: "Create post",
        body : req.body
    });
});

app.get('/post/:id', (req, res) => {
    res.send('GET USER LIST: GET /user');
});

app.patch('/post/:id', (req, res)=>{
    const msg = {
        message : 'UPDATE POST: PATCH /post/' + req.params.id,
        body = req.body
    };
    res.send(msg);
});
app.delete('/post/:id', (req, res) => {
    res.send('DELETE POST: DELETE /post/' + req.params.id);
    });
    

app.listen(port, () =>{
    console.log(`cli-nodejs-api listening at http://localhost:${port}`);
});